const gulp = require('gulp');
const zip = require('gulp-zip');
var fileindex = require('gulp-fileindex');

gulp.task('creacion_artefacto', () =>
    gulp.src("src/*")
        .pipe(zip('webservices.zip'))
        .pipe(gulp.dest('dist'))
);


gulp.task('creacion_directorio', function() {
    return gulp.src("dist/*.zip")
    .pipe(fileindex())
    .pipe(gulp.dest('dist'));
});

gulp.task('default',
  gulp.series('creacion_artefacto','creacion_directorio'));
